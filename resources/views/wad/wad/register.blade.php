<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Register</title>
    <!-- CSS -->
    <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
    <link rel='stylesheet' href='https://npmcdn.com/flickity@2/dist/flickity.css'>

    <link rel="stylesheet" href="{{ asset('assets/style.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/semantic/dist/semantic.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('assets/register.css') }}">
    <!-- JavaScript -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>

    <script src="{{ asset('assets/semantic/dist/semantic.min.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.3/semantic.min.js"></script>


    <script src="{{ asset('assets/java.js') }}" type="text/javascript"></script>
</head>

<body>
    <div class="wrap container">
        <div class="row">
            <div class="col">
                <p class="welcome">Buat akun kamu <br> Sekarang.</p>
                <form id="main" class="ui form" method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="field">
                        <input type="text" name="name" placeholder="Nama kamu" value="{{ old('name') }}">
                        <!-- <input id=" name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus> -->
                    </div>
                    <div class="field">
                        <input type="email" name="email" placeholder="Alamat email kamu" value="{{ old('email') }}" autocomplete="email">
                        <!-- <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email"> -->
                    </div>
                    <div class="field">
                        <input id="password" type="password" name="password" placeholder="Password" required autocomplete="new-password">
                        <!-- <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password"> -->
                    </div>

                    <div class="field">
                        <input id="password_confirmation" type="password" name="password_confirmation" equalTo='#password' placeholder="Confirmation Password" required autocomplete="new-password">
                        <!-- <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password"> -->
                    </div>
                    <script>
                        $(document).ready(
                            function() {
                                $('.ui.checkbox')
                                    .checkbox();
                            });
                    </script>
                    <script>

                    </script>
                    <button type="submit" class="masuk ui primary button">
                        Daftar
                    </button>

                    <img class="atau" src="{{ asset('assets/Assets/atau.png') }}">
                    <button class="hehe ui blue basic button">
                        <i class="google icon"></i>
                        Daftar dengan Google
                    </button>
                    <button class="hehe ui grey basic button">
                        <i class="facebook icon"></i>
                        Daftar dengan Facebook
                    </button>
                    <p class="txtregis">Sudah punya akun?</p>
                    <a class="registext" href="./login.html">Login disini</a>
            </div>
        </div>
        <script>
            $(function() {
                $('#main')
                    .form({
                        name: {
                            identifier: 'name',
                            rules: [{
                                type: 'empty',
                                prompt: 'Maaf, Tolong isi nama kamu dulu'
                            }]
                        },
                        email: {
                            identifier: 'email',
                            rules: [{
                                    type: 'empty',
                                    prompt: 'Maaf, Tolong isi email kamu dulu'
                                },

                            ]

                        },
                        password: {
                            identifier: 'password',
                            rules: [{
                                    type: 'empty',
                                    prompt: 'Maaf, Tolong isi password kamu dulu'
                                },
                                {
                                    type: 'regExp[/.{8,}/]',
                                    prompt: 'Tulis password minimal 8 karakter'
                                }
                            ]
                        },
                        password_confirmation: {
                            identifier: 'password_confirmation',
                            rules: [{
                                    type: 'empty',
                                    prompt: 'Maaf, Tolong isi password kamu dulu'
                                },
                                {
                                    type: 'regExp[/.{8,}/]',
                                    prompt: 'Tulis password minimal 8 karakter'
                                },
                                {
                                    type: 'match[password]',
                                    prompt: 'Confirmation Password Tidak Sama !'
                                },

                            ]
                        },

                    }, {
                        on: 'blur',
                        inline: true,
                        onSuccess: function() {

                            //$(".ui.button[name='account']").removeClass('disabled');
                            //console.log('Success');

                        }
                    });
            });
        </script>
    </div>

    <img class="signnupimg" src="{{ asset('assets/Assets/signup_img@2x.png') }}">
    </div>
    </div>

</body>