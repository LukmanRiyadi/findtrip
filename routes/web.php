<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/landing', function () {
    return view('wad.wad.index');
});
Route::get('/signin', function () {
    return view('wad.wad.login');
});
Route::get('/signup', function () {
    return view('wad.wad.register');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
